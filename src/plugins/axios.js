"use strict";

import Vue from "vue";
import axios from "axios";

let PUBLIC_URL = ['/wallet/initialConf']

let config = {
  timeout: 40 * 1000, // Timeout
};

const _axios = axios.create(config);

_axios.interceptors.request.use(
  function (config) {

    if (PUBLIC_URL.find(a => a === parseURL(config.url).pathname)) {
      return config
    } else {
      config.headers.Authorization =
        "Bearer " + localStorage.getItem('token');
      return config
    }
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
_axios.interceptors.response.use(
  function (response) {
    // Do something with response data
    return response;
  },
  function (error) {
    if (error.response != undefined) {

      if (error.response.status == 401 || error.response.statusText == "Unauthorized") {
        setTimeout(() => {
          error.response.config.headers.Authorization = 'Bearer ' + localStorage.getItem('token');
          axios.request(error.response.config)
        }, 2000)
      }
    }
    return Promise.reject(error);
  }
);

function parseURL(url) {
  var parser = document.createElement('a'),
    searchObject = {},
    queries,
    split,
    i
  // Let the browser do the work
  parser.href = url
  // Convert query string to object
  queries = parser.search.replace(/^\?/, '').split('&')
  for (i = 0; i < queries.length; i++) {
    split = queries[i].split('=')
    searchObject[split[0]] = split[1]
  }
  return {
    protocol: parser.protocol,
    host: parser.host,
    hostname: parser.hostname,
    port: parser.port,
    pathname: parser.pathname,
    search: parser.search,
    searchObject: searchObject,
    hash: parser.hash
  }
}

Plugin.install = function (Vue) {
  Vue.axios = _axios;
  window.axios = _axios;
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return _axios;
      }
    },
    $axios: {
      get() {
        return _axios;
      }
    }
  });
};

Vue.use(Plugin);

export default Plugin;
